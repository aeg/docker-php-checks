# Docker-PHP-Checks

This Docker image is used to run a batch of tests on PHP files. Current tests are :
- PHP lint with `php -l`
- PHP lint with PHPLINT tool : https://github.com/overtrue/phplint
- PHP-CS-Fixer (PHP Coding Standards Fixer, with `--diff --dry-run` only) : https://github.com/PHP-CS-Fixer/PHP-CS-Fixer
- PHPStan (PHP Static Analysis Tool) : https://github.com/phpstan/phpstan

This image is rootless.

## Available Architectures
- amd64
- arm64 (aarch64)
- armv7 (arm)

## Common Usage exemple
```
docker run --rm --name=docker-php-checks -v docker-php-checks:/home/container \
  -e FILES='*.php *.inc' \
  -e DEBUG='true' \
 noxinmortus/docker-php-checks
```

The docker image will execute its checks on files present in `/home/container` (your `docker-php-checks` volume).

## User / Group Identifiers

When using volumes (`-v` flags) permissions issues can arise between the host OS and the container, we avoid this issue by allowing you to specify the user `UID` and group `GID` :
```
-e UID=1000 # for UserID
-e GID=1000 # for GroupID
```

Ensure any volume directories on the host are owned by the same user you specify and any permissions issues will vanish like magic.

In this instance `UID=1000` and `GID=1000`, to find yours use `id user` as below:
```
  $ id username
    uid=1000(dockeruser) gid=1000(dockergroup) groups=1000(dockergroup)
```

## Configuration

Volume to mount is `/home/container` and contains no files by default.

|Environment variables|Default|Usage|
|-|-|-|
|FILES|`*.php`|Files pattern to run check on|
|DEPTH|`2`|Depth of the directory tree to check|
|DEBUG|`false`|Debug mode|
