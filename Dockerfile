#-----------------------------#
# Dockerfile PHP Checks       #
# by NoxInmortus              #
#-----------------------------#

FROM php:7.4-cli-alpine
LABEL maintainer='NoxInmortus'

ENV PHPLINT='https://github.com/overtrue/phplint/releases/download/3.4.0/phplint.phar' \
    PHPCSF='https://cs.symfony.com/download/php-cs-fixer-v3.phar' \
    PHPSTAN='https://github.com/phpstan/phpstan/releases/download/1.10.3/phpstan.phar' \
    USER='container' \
    UID='1000' \
    GID='1000' \
    WORKDIR='/home/container'

COPY entrypoint.sh update-perms.sh /

RUN addgroup --gid ${GID} ${USER} \
    && adduser -D --uid ${UID} -h ${WORKDIR} --ingroup ${USER} ${USER} \
    && wget ${PHPLINT} -nv -O /usr/local/bin/phplint \
    && wget ${PHPCSF} -nv -O /usr/local/bin/php-cs-fixer \
    && wget ${PHPSTAN} -nv -O /usr/local/bin/phpstan \
    && echo '@reboot /update-perms.sh >/dev/null 2>&1' >> /etc/crontabs/root \
    && chmod +x -v /usr/local/bin/php* /update-perms.sh /entrypoint.sh \
    && chown -R ${USER}:${USER} ${WORKDIR} \
    && rm -rfv /tmp/* /var/tmp/* /var/cache/apk/* \
    ;

VOLUME ${WORKDIR}
USER ${USER}
WORKDIR ${WORKDIR}
ENTRYPOINT ["/entrypoint.sh"]
