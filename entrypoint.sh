#!/bin/sh
set -eu

# Make sure we use defaults everywhere.
: "${DEBUG:=false}"
: "${DEPTH:=2}"
: "${FILES:=*.php}"
: "${DEBUG_OPT:=-v}"

SEPARATOR='-----------------------------------------------------------------------------------------'

if [ "${DEBUG}" = 'true' ]; then
  php --version
  php -i
  php -m
  DEBUG_OPT='-vvv'
fi

for file in ${FILES}; do
  echo "${SEPARATOR} Running php -l for ${file} ${SEPARATOR}"
  find "${WORKDIR}" -maxdepth "${DEPTH}" -name "${file}" -type f -exec php -l {} \;

  echo "${SEPARATOR} Running phplint for ${file} ${SEPARATOR}"
  find "${WORKDIR}" -maxdepth "${DEPTH}" -name "${file}" -type f -exec phplint "${DEBUG_OPT}" {} \;

  echo "${SEPARATOR} Running php-cs-fixer for ${file} ${SEPARATOR}"
  find "${WORKDIR}" -maxdepth "${DEPTH}" -name "${file}" -type f -exec php-cs-fixer fix "${DEBUG_OPT}" --diff --dry-run {} \;
done

echo "${SEPARATOR} Running phpstan ${SEPARATOR}"
phpstan analyze --memory-limit 128M "${DEBUG_OPT}" .

eval "${@}"
